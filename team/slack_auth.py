import requests
import json

CLIENT_ID = "1950703337543.1976204162676"
CLIENT_SECRET = "4e8dd84d1c2c0c2f021fcd45796dc71c"
SLACK_URL = ""

# slack の認証情報を利用してこちらのサイトに入ってもらうために　slack channel　上での情報を取得する

class Auth(object):
    def __init__(self):
        self.client_id = CLIENT_ID
        self.client_secret = CLIENT_SECRET
    
    def get_user_info(self, request):
        if request.method == "GET":
            code = request.GET["code"]
            payload = {
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "code": code,
            }
            url = "https://slack.com/api/oauth.v2.access?client_id={}&client_secret={}&code={}".format(payload["client_id"], payload["client_secret"], payload["code"])
            res = requests.get(url)
            json = res.json(res)
            return json

        
