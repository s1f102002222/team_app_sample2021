import requests
import json

class TMDB(object):
    def __init__(self, token):
        self.token = token
        self.headers_ = {'Authorization': f'Bearer {self.token}', 'Content-Type': 'application/json;charset=utf-8'}
        self.base_url_ = 'https://api.themoviedb.org/3/'
        self.img_base_url_ = 'https://image.tmdb.org/t/p/w500'
    def _json_by_get_request(self, url, params={}):
        res = requests.get(url, headers=self.headers_, params=params)
        return json.loads(res.text)

    def search_movies(self, query):
        params = {'query': query}
        url = f'{self.base_url_}search/movie'
        return self._json_by_get_request(url, params)

    def get_movie(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}'
        return self._json_by_get_request(url)

    def get_movie_account_states(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/account_states'
        return self._json_by_get_request(url)

    def get_movie_alternative_titles(self, movie_id, country=None):
        url = f'{self.base_url_}movie/{movie_id}/alternative_titles'
        return self._json_by_get_request(url)

    def get_movie_changes(self, movie_id, start_date=None, end_date=None):
        url = f'{self.base_url_}movie/{movie_id}'
        return self._json_by_get_request(url)

    def get_movie_credits(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/credits'
        return self._json_by_get_request(url)

    def get_movie_external_ids(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/external_ids'
        return self._json_by_get_request(url)

    def get_movie_images(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/images'
        return self._json_by_get_request(url)

    def get_movie_keywords(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/keywords'
        return self._json_by_get_request(url)

    def get_movie_release_dates(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/release_dates'
        return self._json_by_get_request(url)

    def get_movie_videos(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/videos'
        return self._json_by_get_request(url)

    def get_movie_translations(self, movie_id):
        url = f'{self.base_url_}movie/{movie_id}/translations'
        return self._json_by_get_request(url)

    def get_movie_recommendations(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/recommendations'
        return self._json_by_get_request(url)

    def get_similar_movies(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/similar'
        return self._json_by_get_request(url)

    def get_movie_reviews(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/reviews'
        return self._json_by_get_request(url)

    def get_movie_lists(self, movie_id, language=None):
        url = f'{self.base_url_}movie/{movie_id}/lists'
        return self._json_by_get_request(url)

    def get_latest_movies(self, language=None):
        url = f'{self.base_url_}movie/latest'
        return self._json_by_get_request(url)

    def get_now_playing_movies(self, language=None, region=None):
        url = f'{self.base_url_}movie/now_playing'
        return self._json_by_get_request(url)

    def get_popular_movies(self, language=None, region=None):
        url = f'{self.base_url_}movie/popular'
        return self._json_by_get_request(url)

    def get_top_rated_movies(self, language=None, region=None):
        url = f'{self.base_url_}movie/top_rated'
        return self._json_by_get_request(url)

    def get_upcoming_movies(self, language=None, region=None):
        url = f'{self.base_url_}movie/upcoming'
        return self._json_by_get_request(url)

class Title(object):
    def __init__(self):
        self.api_token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5ZDQ3YjJjNDlkZmUzYTFlN2MyMWEzYTM5YzNjYTgzNCIsInN1YiI6IjYwNzlkMmE2Nzc3NmYwMDA1ODE4M2NhMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.sk4Rd7NbCbRPaJXcsRqS05YmTpztmM5OgQ6dR5g7Dno'
    
    def title_set_popular(self):
        api = TMDB(self.api_token)
        pop_mov = api.get_popular_movies()
        infos = self.roop(pop_mov)
        return infos
    
    def set_search_movies(self, title):
        api = TMDB(self.api_token)
        movies = api.search_movies(title)
        infos = self.roop(movies)
        return infos
    
    def get_top5_movies(self):
        api = TMDB(self.api_token)
        pop_mov = api.get_popular_movies()
        results = pop_mov["results"]
        base_img_url = api.img_base_url_
        infos = {}
        for movie in results:
            img_url = base_img_url+movie["backdrop_path"]
            vote_average = self.convert_rate(movie["vote_average"])
            infos[movie['original_title']] = [vote_average, img_url, movie["overview"]]
        top_movies = sorted(infos.items(), key = lambda x: x[1])
        return top_movies
    
    def get_upcomming(self):
        api = TMDB(self.api_token)
        res = api.get_upcoming_movies()
        infos = self.roop(res)
        return infos
    
    def get_movie_ids(self, Id):
        api = TMDB(self.api_token)
        response = api.get_movie_videos(Id)
        response = response["results"]
        try:
            res = response[0]
            return res["key"]
        except IndexError as err:
            return None
    
    def get_similar(self, title):
        api = TMDB(self.api_token)
        try:
            first_movie = api.search_movies(title)["results"][0]
            Id = first_movie["id"]
            similar_movies = api.get_similar_movies(Id)
            infos = self.roop(similar_movies)
            return infos
        except:
            return None


    # ループのサブルーチン
    
    def roop(self, result):
        infos = {}
        index = 0
        for movie in result["results"]:
            vote_average = self.convert_rate(movie["vote_average"])
            youtube_key = self.get_movie_ids(movie["id"])
            if youtube_key == None:
                youtube_key = ""
            infos[index] = {
                "img_url": movie["backdrop_path"],
                "rate": vote_average,
                "release_date": movie["release_date"],
                "title": movie["original_title"],
                "overview": movie["overview"],
                "youtube_url": "https://www.youtube.com/watch?v={}&t=25s$data.items[0].snippet.channelId".format(youtube_key)
            }
            index += 1
        return infos    

    # convert_rateによって10.00が最大のスコアのところを５.00を最大のスコアとした時の値を返す
    # "小数第一位までで四捨五入" に変更

    def convert_rate(self, score):  
        vote_average = ((score*0.5)+0.05)*10 //1 /10
        return vote_average