import requests
import json

class Youtube(object):
    def __init__(self):
        self.youtube_api_key = "AIzaSyB2ZmP4ihw1Yb63ZnRHWBtFNM6n7llwFgY"
    
    def get_video_info(self, video_id, mode="default"):
        url = "https://www.googleapis.com/youtube/v3/videos?id={}&key={}&part=snippet,contentDetails,statistics,status".format(video_id, self.youtube_api_key)
        res = requests.get(url)
        json = res.json()
        items = json["items"]
        snippet = items[0]["snippet"]
        thumbnail_default = snippet["thumbnails"]["default"]
        thumbnail_medium = snippet["thumbnails"]["medium"]
        thumbnail_standard = snippet["thumbnails"]["standard"]
        thumbnail_maxres = snippet["thumbnails"]["maxres"]
        
        if mode == "default":
            return thumbnail_default
        elif mode == "medium":
            return thumbnail_medium
        elif mode == "standard":
            return thumbnail_standard
        elif mode == "maxres":
            return thumbnail_maxres
