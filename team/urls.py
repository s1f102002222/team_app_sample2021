from django.urls import path, include
from . import views

app_name = "nattou"
urlpatterns = [
    path('', views.login, name="top"),
    path('home/', views.HomeView.as_view(), name="home"),
    path('search', views.SearchMovie.as_view(), name="search"),
    path('good/<int:index>', views.send_movie_message, name="good"),
    path('info/<int:index>', views.HomeView.as_view(), name="info"),
]