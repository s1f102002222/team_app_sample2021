from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from urllib.parse import urlencode
import urllib
import json
from .movie import TMDB, Title
from .slack_auth import Auth
from django.contrib.auth.models import User
from account.models import Account

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from .youtube import Youtube

import requests
import os
import random

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'rHJmMZnxK2PpCEcBPjeGV3T0'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'
SLACK_BOT_TOKEN = "xoxb-1950703337543-1955290934615-JBA4qXoRISYFGiK7NBomYDSS"
CHANNEL_ID = "C01UB7CA7FG"
YOUTUBE_API = "AIzaSyB2ZmP4ihw1Yb63ZnRHWBtFNM6n7llwFgY"


API = TMDB('eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5ZDQ3YjJjNDlkZmUzYTFlN2MyMWEzYTM5YzNjYTgzNCIsInN1YiI6IjYwNzlkMmE2Nzc3NmYwMDA1ODE4M2NhMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.sk4Rd7NbCbRPaJXcsRqS05YmTpztmM5OgQ6dR5g7Dno')

test_template_1 = "test_page_1.html"

popular_movies = []



# def index(request):
#     positive_replies = Reply.objects.filter(response=Reply.POSITIVE)
#     neutral_replies = Reply.objects.filter(response=Reply.NEUTRAL)
#     negative_replies = Reply.objects.filter(response=Reply.NEGATIVE)

#     context = {
#         'positive_replies': positive_replies,
#         'neutral_replies': neutral_replies,
#         'negative_replies': negative_replies,

#     }
#     return render(request, 'index.html', context)

# def clear(request):
#     Reply.objects.all().delete()
#     return redirect(index)

# def announce(request):
#     if request.method == 'POST':
#         data = {
#             'text': request.POST['message']
#         }
#         post_message(WEBHOOK_URL, data)

#     return redirect(index)

# @csrf_exempt
# def echo(request):
#     if request.method != 'POST':
#         return JsonResponse({})
    
#     if request.POST.get('token') != VERIFICATION_TOKEN:
#         raise SuspiciousOperation('Invalid request.')
    
#     user_name = request.POST['user_name']
#     user_id = request.POST['user_id']
#     content = request.POST['text']

#     result = {
#         'text': '<@{}> {}'.format(user_id, content.upper()),
#         'response_type': 'in_channel'
#     }

#     return JsonResponse(result)

# @csrf_exempt
# def hello(request):
#     if request.method != 'POST':
#         return JsonResponse({})
    
#     if request.POST.get('token') != VERIFICATION_TOKEN:
#         raise SuspiciousOperation('Invalid request.')
    
#     user_name = request.POST['user_name']
#     user_id = request.POST['user_id']
#     content = request.POST['text']

#     result = {
#         'blocks': [
#             {
#                 'type' : 'section',
#                 'text' : {
#                     'type': 'mrkdwn',
#                     'text': '<@{}> How are you?'.format(user_id)
#                 },
#                 'accessory': {
#                     'type': 'static_select',
#                     'placeholder': {
#                         'type': 'plain_text',
#                         'text': 'I am:',
#                         'emoji': True
#                     },
#                     'options': [
#                         {
#                             'text': {
#                                 'type': 'plain_text',
#                                 'text': 'Fine.',
#                                 'emoji': True
#                             },
#                             'value': 'positive'
#                         },
#                         {
#                             'text': {
#                                 'type': 'plain_text',
#                                 'text': 'So so.',
#                                 'emoji': True
#                             },
#                             'value': 'neutral'
#                         },
#                         {
#                             'text': {
#                                 'type': 'plain_text',
#                                 'text': 'Terrible.',
#                                 'emoji': True
#                             },
#                             'value': 'negative'
#                         }
#                     ],
#                     'action_id': ACTION_HOW_ARE_YOU
#                 }
#             }
#         ],
#         'response_type': 'in_channel'
#     }

#     return JsonResponse(result)

# @csrf_exempt
# def reply(request):
#     if request.method != 'POST':
#         return JsonResponse({})
    
#     payload = json.loads(request.POST.get('payload'))
#     print(payload)
#     if payload.get('token') != VERIFICATION_TOKEN:
#         raise SuspiciousOperation('Invalid request.')
    
#     if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
#         raise SuspiciousOperation('Invalid request.')
    
#     user = payload['user']
#     selected_value = payload['actions'][0]['selected_option']['value']
#     response_url = payload['response_url']

#     if selected_value == 'positive':
#         reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
#         reply.save()
#         response = {
#             'text': '<@{}> Great! :smile:'.format(user['id'])
#         }
#     elif selected_value == 'neutral':
#         reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
#         reply.save()
#         response = {
#             'text': '<@{}> Ok, thank you! :sweat_smile:'.format(user['id'])
#         }
#     else:
#         reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
#         reply.save()
#         response = {
#             'text': '<@{}> Good luck! :innocent:'.format(user['id'])
#         }
    
#     post_message(response_url, response)

#     return JsonResponse({})

# このメソッドは使える
def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()

@csrf_exempt
def eat(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    meal = ['ラーメン','カレー','オムライス']
    n  = random.randint(0,len(meal)-1)
    result = {
        'text' : '<@{}> {}'.format(user_id, meal[n]),
        'response_type': 'in_channel' 
    }
    return JsonResponse(result)

@csrf_exempt
def find(request):
    if request.method != 'POST':
        return JsonResponse({})

    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']

    title = Title()
    top5_movies = title.get_top5_movies()
    top5_movies_title = [title[0] for title in top5_movies][::-1][:5]
    top5_movies_urls = [ele[1][1] for ele in top5_movies][::-1][:5]
    top5_movies_rates = [ele[1][0] for ele in top5_movies][::-1][:5]
    top5_movies_overview = [ele[1][2] for ele in top5_movies][::-1][:5]

    payload = {
         'blocks': [
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '人気の映画上位5件を検索しました。'
                }
            },
            {
                'type': 'divider'
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*{}*\n{}'.format(top5_movies_title[0], top5_movies_overview[0])
                },
                'accessory': {
                    'type': 'image',
                    'image_url': "{}".format(top5_movies_urls[0]),
                    'alt_text': 'Movie Poster Image'
                }
            },
            {
                'type': 'section',
                'fields': [
                    {
                        'type': 'mrkdwn',
                        'text': '*Average Rating*\n{}'.format(top5_movies_rates[0])
                    }
                ]
            },
            {
                'type': 'divider'
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*{}*\n{}'.format(top5_movies_title[1], top5_movies_overview[1])
                },
                'accessory': {
                    'type': 'image',
                    'image_url': '{}'.format(top5_movies_urls[1]),
                    'alt_text': 'Movie Poster Image'
                }
            },
            {
                'type': 'section',
                'fields': [
                    {
                        'type': 'mrkdwn',
                        'text': '*Average Rating*\n{}'.format(top5_movies_rates[1])
                    }
                ]
            },
            {
                'type': 'divider'
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*{}*\n{}'.format(top5_movies_title[2], top5_movies_overview[2])
                },
                'accessory': {
                    'type': 'image',
                    'image_url': '{}'.format(top5_movies_urls[2]),
                    'alt_text': 'Movie Poster Image'
                }
            },
            {
                'type': 'section',
                'fields': [
                    {
                        'type': 'mrkdwn',
                        'text': '*Average Rating*\n{}'.format(top5_movies_rates[2])
                    }
                ]
            },
            {
                'type': 'divider'
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*{}*\n{}'.format(top5_movies_title[3], top5_movies_overview[3])
                },
                'accessory': {
                    'type': 'image',
                    'image_url': '{}'.format(top5_movies_urls[3]),
                    'alt_text': 'Movie Poster Image'
                }
            },
            {
                'type': 'section',
                'fields': [
                    {
                        'type': 'mrkdwn',
                        'text': '*Average Rating*\n{}'.format(top5_movies_rates[3])
                    }
                ]
            },
            {
                'type': 'divider'
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*{}*\n{}'.format(top5_movies_title[4], top5_movies_overview[4])
                },
                'accessory': {
                    'type': 'image',
                    'image_url': '{}'.format(top5_movies_urls[4]),
                    'alt_text': 'Movie Poster Image'
                }
            },
            {
                'type': 'section',
                'fields': [
                    {
                        'type': 'mrkdwn',
                        'text': '*Average Rating*\n{}'.format(top5_movies_rates[4])
                    }
                ]
            }
        ]
    }
    
    return JsonResponse(payload)

# def login_check(request):
#     code = request.GET["code"]
#     user_id = code[""]
#     return code != None

def login_page_view(request):
    if request.method == "GET":
        return render(request, "loginpage.html")

@csrf_exempt
def category(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_id = request.POST['user_id']

    r  = random.randint(1,10)
    if r == 1:
        result = {
        'text': '<@{}>さんアクション映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 2:
        result = {
        'text': '<@{}>さんアニメ映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 3:
        result = {
        'text': '<@{}>さんSF映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 4:
        result = {
        'text': '<@{}>さん短編映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 5:
        result = {
        'text': '<@{}>さんファミリー映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 6:
        result = {
        'text': '<@{}>さんドラマ映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 7:
        result = {
        'text': '<@{}>さんファンタジー映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 8:
        result = {
        'text': '<@{}>さんミュージカル映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    elif r == 9:
        result = {
        'text': '<@{}>さんホラー映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }
    else:
        result = {
        'text': '<@{}>さんスポーツ映画はどうですか？'.format(user_id),
        'response_type': 'in_channel'
     }


    return JsonResponse(result)

def send_movie_message(request, index):
    if request.method == "POST":
        title = Title()
        titles_url = title.title_set_popular()
        base_img_url = API.img_base_url_
        movies = titles_url[index]
        text = {
            'blocks': [
                {
                    'type': 'section',
                    'text': {
                        'type': 'mrkdwn',
                        'text': '{}\nという映画がいいねされました。\n{}'.format(movies["title"], movies["overview"])
                    },
                    'accessory': {
                        'type': 'image',
                        'image_url': '{}'.format(base_img_url + movies["img_url"]),
                        'alt_text': 'Movie Poster Image'
                    }
                },
                {
                    'type': 'section',
                    'fields': [
                        {
                            'type': 'mrkdwn',
                            'text': '*Average Rating*\n{}'.format(movies["rate"])
                        }
                    ]
                },
                {
                    'type': 'divider'
                },
            ]
        }
        post_message(WEBHOOK_URL, text)
    return redirect("nattou:home")

def get_slack_account_code(request):
    try:
        code = request.GET["code"]
        payload = {
            "client_id": "1950703337543.1976204162676",
            "client_secret": "4e8dd84d1c2c0c2f021fcd45796dc71c",
            "code": code
            }
        url = "https://slack.com/api/oauth.v2.access?client_id={}&client_secret={}&code={}".format(payload["client_id"], payload["client_secret"], payload["code"])
        res = requests.get(url)
        res = res.json()
        user_id = res["authed_user"]["id"]
        return res, user_id
    except KeyError as err:
        return None, None

def get_movie_info(user_id=None):
    title = Title()
    titles_url = title.title_set_popular()
    base_img_url = API.img_base_url_
    res = title.get_upcomming()
    context = {
        "titles": titles_url,
        "base_img_url_": base_img_url,
        "movies": res,
        "user_id": user_id
    }
    return context

def login(request):
    return render(request, 'loginpage.html')
      

class HomeView(TemplateView):
    def get(self, request, *args, **kwargs):
        context = get_movie_info()
        return render(request, 'home.html', context=context)

    # def post(self, request, *args, **kwargs):
    #     form = UserChangeForm(request.POST or None, instance=request.user)
    #     if form.is_valid():
    #         form.save()
    #         return redirect("nattou:home")
    #     context = {
    #         "form":form,
    #     }
    #     return render(request, '')
    
class SearchMovie(TemplateView):
    def get(self, request, *args, **kwargs):
        return render(request, "search.html")

    def post(self, request, *args, **kwargs):
        title = Title()
        if request.POST["search"] != "":
            movies_url = title.set_search_movies(request.POST["search"])
            similar_movie = title.get_similar(request.POST["search"])
            base_img_url = API.img_base_url_
            context = {
                'movies': movies_url,
                'similar': similar_movie,
                'base_img_url_': base_img_url,
                }
            return render(request, "search.html", context)
        elif request.POST["search"] == "":
            return redirect("nattou:home")

class New(TemplateView):
    def post(self, request, *args, **kwargs):
        form = UserChangeForm(request.POST or None)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            return redirect('nattou:home')
        context = {
            'form': form,
        }
        return render(request, 'form.html', context)

class Change_Data(LoginRequiredMixin, TemplateView):
    def post(self, request, *args, **kwargs):
        form = UserChangeForm(request.POST or None, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('nattou:home')
        context = {
            'form': form,
        }
        return render(request, 'change_data.html', context)
    
class Change_Password(LoginRequiredMixin, TemplateView):
    def post(self, request, *args, **kwargs):
        form = UserPasswordChangeForm(request.user, request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('nattou:home')
        context = {
            'form': form,
        }
        return render(request, 'change_password.html', context)


        

