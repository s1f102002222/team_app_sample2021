from django.urls import path, include
from django.contrib.auth.views import LoginView
from django.contrib.auth import views as auth_views
from . import views

app_name = 'account'
urlpatterns = [
    path('', views.Login.as_view(),name='login'),
    path("logout", views.Logout.as_view(), name="logout"),
    path('register', views.AccountRegistration.as_view(), name='register'),
    
]