from django.shortcuts import render, redirect
from django.views.generic import TemplateView, FormView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import View
from .forms import AccountForm, AddAccountForm
from .models import Account
import requests
import json

class AccountRegistration(TemplateView):

    def __init__(self):
        self.params = {
            "AccountCreate": False,
            "account_form": AccountForm(),
            "add_account_form":AddAccountForm(),
        }
    
    def get(self, request):
        self.params["account_form"] = AccountForm()
        self.params["add_account_form"] = AddAccountForm()
        self.params["AccountCreate"] = False
        return render(request, "login/register.html", context=self.params)
    
    def post(self, request):
        self.params["account_form"] = AccountForm(data=request.POST)
        self.params["add_account_form"] = AddAccountForm(data=request.POST)

        code = request.GET["code"]
        payload = {
            "client_id": "1950703337543.1976204162676",
            "client_secret": "4e8dd84d1c2c0c2f021fcd45796dc71c",
            "code": code
        }
        url = "https://slack.com/api/oauth.v2.access?client_id={}&client_secret={}&code={}".format(payload["client_id"], payload["client_secret"], payload["code"])
        res = requests.get(url)
        res = res.json()
        slack_api = res["authed_user"]["id"]
           
        

        if self.params["account_form"].is_valid() and self.params["add_account_form"].is_valid():
            account = self.params["account_form"].save()
            account.set_password(account.password)
            account.save()
            
            add_account = self.params["add_account_form"].save(commit=False)
            add_account.user = account

            if Account.objects.filter(slack_id=slack_api).exists():
                return render(request, 'login/register.html', context=self.params)

            add_account.slack_id = slack_api

            if 'account_image' in request.FILES:
                add_account.account_image = request.FILES['account_image']
            add_account.save()
        else:
             print(self.params["account_form"].errors)
        
        return render(request, "login/register.html",context=self.params)

class Login(TemplateView):
    def post(self, request, *args, **kwargs):
        ID = request.POST.get('userid')
        Pass = request.POST.get('password')

        user = authenticate(username=ID, password=Pass)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('nattou:home'))
            else:
                return HttpResponse("アカウントが有効ではありません")
        else:
            return HttpResponse('ログインIDまたはパスワードが間違っています')
    
    def get(self, request, *args, **kwargs):
        return render(request, 'login/login.html')

class Logout(LoginRequiredMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('account:login'))